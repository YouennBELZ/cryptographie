# OpenSSL
Librairie opensource qui permet de faire tout ce qu'on viens de dire et bien plus encore
- Générer des clé publique et privé RSA
- Chiffrement symétrique et asymétrique
- Chiffrer en AES avec ECB ou CBC
- Générer des certificats X.509 et des CSR (Certificate Signing Requests) (connexion HTTPS)
- Déchiffrement également
- SSL/TLS pour la **sécurisation** des connexions HTTP, SMTP, IMAP
- Utilisé pour **signer** et **vérifier** des données numériquement


- Génération de clés RSA
```
openssl genpkey -algorithm RSA -out private_key.pem
```