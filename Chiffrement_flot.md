# Chiffrement par flot

## Principes
A partir d'une clé, génération d'une suite chiffrante
**peusdo-aléatoire**. Ensuite combinaison de cette suite,
avec les données à chiffrer.


### Protocoles exemples
- WEP
- WPA
- WPA2
- E0 : Bluetooth
- A5 : GSM

WPA / 2 WEP A5 Bluetooth GSM
LFSR : Très rapide, permet de chiffrer au fur et à mesure (efficace en mémoire)

## Caractéristiques
- Rapide
- Implémentation matérielles efficaces
- Adapté aux applications temps réel

## Problèmes
 - Même aléa quand on utilise la même clé :

```
M1 (xor) K = C1

M2 (xor) K = C2

C1 (xor) C2 = M1(xor)M2(xor)K(xor)K
			= M1 (xor) 
```

Pour éviter ça dans le nombre pseudo aléatoire on la compose :
- la clé
- l'initialisation vector (IV)
Les vecteur sont de 2¹⁶ ca fait 1 chance sur 50 000 que deux messages successive ai une même IV.
**Paradox des anniversaires** : 365 date, proba même anniv faible. 25 personnes 50%, 50 personne 99.9 %

Sur 100 message, il y a une énorme chance qu'il y est un IV.

- Le générateur doit être de bonne qualité, sans corrélation, équilibré (demande du temps de calcul)

La **sécurité** des chiffrements à flot reste **faible**.
- WEP : génère une clé trop simple
	- attaque par fragmentation de paquet en qq secondes (2006)
