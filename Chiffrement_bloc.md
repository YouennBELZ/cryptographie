# Chiffrement symétrique par bloc
1. On découpe des fichier en **blocs** de taille fixe
2. On chiffre chaque bloc
3. En utilisant un **mode opératoire**, on combine les blocs avec un mode de chiffrement

- On peut faire des opérations matricielles
- Affecter le chiffrement du bloc suivant en fonction du bloc précédent

**Inconvénient principale :** techniquement compliqué de mettre en place un mode opératioire

# AES
Chiffrement symétriques les plus utilisées ajd

AES se décline en trois versions :
- AES-128 > 128 bits, 10 tours
- AES-192 > 192 bits, 12 tours
- AES-256 > 256 bits, 14 tours

(Mathématique qu'on voit en troisième année de cycle universitaire)
Basé sur les corps, ensemble mathématique (comme un espace vectorielle),
on peux créer un corps de polynômes avec des coefficient de 1 ou 0 (nombre de binaire) quotientisé qui donne un ensemble qui donne une structure qui contiennent des opérations algébriques qui permette d'avoir des méthodes de chiffrements robustes.

## Sécurisé
Très bien sécurisé, très peux gourmant en mémoire, la meilleure attaque sur 128 bits.
2¹²⁶ au lieu de 2¹²⁸ par attaque par force brute on gagne uniquement que 2 bits sur AES 128.
Les vulnérabilités viennent plus de la mise en place que de l'algorithme

# Fonctionnement
1. On coupe le message en taille 128
2. Chaque bloc de 128 bits est découpé en 16 octets
3. Les 16 octets sont ensuite placés dans une matrice 4X4
4. On réalise 10,12,14 tours d'opérations sur cette matrice :
	1.  On fait un XOR avec des sous-clés
	2. Substitution (opérateur non linéaire sur chaque octet)
	3. Décalage de lignes + Mélange de colonnes (**opération matricielle** par bloc)
5. On mélange ces blocs chiffrés grâce à un **mode opératoire** 
ECB on met tout les blocs les uns à la suite des autres
CBC on fait des xor entre les blocs