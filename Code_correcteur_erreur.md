## Code détecteur / correcteur d'erreur
Est un ensemble de règle algorithmique utilisé pour **détecter** et **corriger** les **erreurs de transmission** dans les données numériques.

"Code" sous etends :
- Code pour la cryptograhie
- Code pour la compression
- Code pour la correction d'erreurs

**Exemple 1**
Paquets IP , détection des erreurs, La correction est alors réalisée par une nouvelle demande de transmission (protocole TCP)

**Exemple 2**
Phillips rayure sur un cd, permet quand meme de lire la données pour une rayure suffisamment faible (0.2 millimètre). (4096 bits consécutif corrigé).

### Code de parité
On ajoute à 7 bits de données **1 bit** valant 1 s'il y a un nombre impair et 0 sinon.
Si à la réception des 8 bits est erroné, il y a détection d'erreur

### Code de k répétition
On **répète** *k* fois chaque bit ou paquet.
`k > 2`
Le k optimal va dépendre de la probabilité qu'un bit soit changé.
Très coûteux en **mémoire**

### Contrôle de redondance cyclique (CRC)
On prend notre stream de bit
Chaque  0 et 1 va nous donner un polynômes (les coefficients de nôtre polynômes)
On ajoute à la séquence binaire le reste d'une division polynomiale. A la **réception** le **reste de la division** reçu et **le reste de la division calculé** doivent coïncider.



**Un bon code doit avoir**
- **Rendement** un grand nombre de bits d'information par rapport aux bits codés
- **Détection et correction** en bonne capacité
- Une **procédure** de décodage (et de codage) **simple** et **rapide**

**Code correcteurs d'erreurs utilisés**
- **Reed-Solomon** : CD / DCD : QR codes
- **BCH** : Communications numériques, cartes à puce
- **Convolutifs** : Systèmes de communication à haut débit, réseaux mobiles, satellites



### Code de Hamming
`a = 1001`

Position qui sont pas des puissances deux :
`u = __1_101`
Calcul des trois bits manquants (XOR):
`
`u1=1+1+1=1`
`u2=1+0+1=0`
`u2=1+0+1=0`

Si on reçoit v comme message erroné
`s1 = v1+v3+v5+v7, s2=v2+v3+v6+v7 s3= v4+v5+v6+v7` 
Si ce syndrome est non nul, il y a eu une erreur de transmission, le syndrome indique la position de l'erreur.


## Le TP
Question 1. Implémenter un fonction prenant en entrée un message a et renvoyant le résultat après application du code de parité.
```python
import re

def code_parite_in(a : str) :
    if re.match(r'^[01]+$', a) :
        nb_1 = a.count('1')

        if nb_1 % 2 == 0 :
            a += '0'
        else :
            a+= '1'

        return a 
    else :
        print("Erreur : le message contient pas que des 0 et 1")

message_1 = '01101101'
message_2 = '10101100'
print(code_parite_in(message_1))
print(code_parite_in(message_2))
```

Out
```shell
011011011
101011000
```

Question 2. Proposer et implémenter une fonction se basant sur le code de parité et étant plus per-
formant pour la détection d’une multitude d’erreurs.
1. Faire des bits de parité à intervalle régulier
2. Faire des modulo 2^k
	1. Modulo 2 (0 1)
	2. Modulo 2^k = 1001....1 (entier que je transforme en binaire)
	On rajoute ce suffixe. Problème si il y a 2^p erreur, on aura le même modulo, si j'ai autant de zéro qui ce sont transformé en 1 que de 1 en zéro.

Question 3. Implémenter un fonction prenant en entrée un message a et un paramètre k et renvoyant
le résultat après application du code de k répétitions.

```python
def code_repetition_in(a : str, k : int) :
	if re.match(r'^[01]+$', a) :
		return a * k
	else :
		print("Erreur : le message contient pas que des 0 et 1")

message_3 = '0101'
print(code_repetition_in(message_3, 4))
```

Out
```shell
0101010101010101
```

Question 4. Implémenter un fonction prenant en entrée un message a et renvoyant le résultat de après application du code de Hamming
