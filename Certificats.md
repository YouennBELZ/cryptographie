# Signatures, certificats et PKI

**Signature électronique :** introduite par Diffie Hellman en 1976, assure **intégrité**, **authentification**. On peut **empêcher** la falsification et donc induire la **non-répudiation**.

Une signature doit être :
- Rapide à calculer
- Vérifiable par le destinataire
- Vérifiable par un tiers
- Infalsifiable, inaltérable
- Inimitable, non-réutilisable
- Non-répudiable

Les signature garantissent donc simultanément l'**authentification**, l'**intégrité** et la **non-répudiation**

## Chiffrement RSA
1. Alice et Bob conviennent d'une fonction de hachage *h*
2. Alice dispose d'une clef privée *d* et d'une clef publique *(e, n)*
3. Alice calcule l'empreinte *h(M)* de son message *M*
4. Alice signe l'empreinte *h(M)* par la signature S = *h(M)^d* mod n et envoie à *Bob* le couple (M,S)

-> La on chiffre avec notre clé privée (celle de Alice), ça assure que la signature ne peux pas être falsifier
Si M est altéré on aura qqchose de différent et la clé privée possède bien celle d'Alice

Quand on reçoit un message Bob déchiffre avec la clé publique d'Alice et compare les hash
M est authentique si *h(M) = S^e mod n*

## Certificats numériques
- Clé publique
- Info propriétaire, nom adresse
- Informations sur le certificat
- Autorité de certification 

**Deux standards**
**X.509** : UIT (Union Internationale des Télécommunications). Fonctionne sur la chaîne de fonctionne, hiérarchie des autorité de certification. 

**OpenPGP** créé en 1998 par l'IETF signer un certificat grâce à plusieurs autres certificats

## pki
L'ensemble des composants, fonctions et procédures mis en place pour la création, la gestion ou la révocation des certificats à clefs publiques


- **Autorité d'enregistrement** (RA)
- **Autorité de certification** (CA)
- **Autorité de validation** (VA)
- **Archivage**

La création d'un certificats dans une PKI se fait alors en envoyant un formulaire à l'**Autorité d'enregistrement**, qui vérifie les informations et la transmet à l'**Autorité de certification**.

## Question 1
*On considère une clé privé d = 23 et une clé public (e, n) = (7, 55). Puis, en suivant le protocole détaillé*
*dans le cours, envoyer un message privé et la signature RSA associé à votre binôme.*
*Un fois avoir reçu le message et la signature que votre binôme vous aura envoyé, vérifier que la signature associé au message est correcte, et donc que le message est authentique.*
Cf : script.py
## Question 2
*Grâce à OpenSSL, générer un couple clé privé clé public, puis toujours en utilisant OpenSSL, générer une signature d’un message en utilisant votre clé privé*

```
openssl genrsa -out private_key.pem 2048
openssl rsa -pubout -in private_key.pem -out public_key.pem

echo -n "Votre message ici" > message.txt
openssl dgst -md5 -sign private_key.pem -out signature.bin message.txt
```

**Vérifier**

```
openssl dgst -md5 -verify public_key.pem -signature signature.bin clear.txt
```


## Question 3
Nous allons créer un certificat au format X.509 auto-signé. Pour cela suivez les étapes suivantes.
1. Générer votre paire de clés RSA dans un fichier nommé "maCle.pem", d’une taille de 1024 bits et protégée par un mot de passe.
2. Créer un fichier ne contenant que la partie publique de votre clé RSA. Dans la suite, on suppose
ce fichier nommé maClePublique.pem.
3. Avec la commande req d’OpenSSL, et à l’aide de votre clé privé, créer un certificat auto-signé.
Ce certificat devra être au format X-509 et valable 365 jours.
En utilisant la commande x509 d’OpenSSL, vous pouvez visualiser en clair les informations du certificat
créé. Examiner son contenu.