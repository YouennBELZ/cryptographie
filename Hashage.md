Fonction de hachage prend en entrée une donnée de **taille variable** et produit une empreinte de taille fixe

Le calcul du hash se fait en temps **polynomial** en la taille du message.

- **MD5** (Message Digest Algorithm 5)
- **SHA-1** (Secure Hash Algorithm 1)
- **SHA-256** (Secure Hash Algorithm 256 bits)

Problème avec MD5, même taille en entrée et en sortie. 2²⁵⁶ possibilité. Paradoxe des anniversaires. 2³⁰ fichier même image haché est élevé.

On augmente le nombre de bit pour éviter le nombre de collision.


**Stockage des mdp**
1. Utilisateur fournis sont **mdp**
2. Le système **hache** le **mdp**
3. Pour se connecter le site web **hache le mdp** et le **compare** à celui stocké.

### Problème attaque rainbow table
Compare les mdp les plus fréquent en claire avec leur hache.
*Pour palier à ce probleme SALT :* Une liste de bits
Le mdp est **concaténé** avec le **sel**
Le résultat est **haché** et **stocké** dans la base de données


#### Question1. Utilisez OpenSSL pour générer des hachages MD5, SHA-1 et SHA-256 pour différents mots ou phrases. Comparer les longueurs des hachages générés.

Hachage de "password" :
**MD5**
```python
$ openssl passwd -1 "password"
$1$oenYHdrD$qHrLDel2u/X4JAxX/wt1G0
```

**SHA-256**
```python
$ openssl passwd -5 "password"       
$5$x9.t0AS7Bx/CGDcK$Pfd6MXj5CJ3QvYm7JrjuxdD6wn0w6tv9UAEXeD3TRM1
```

**SHA-1**
```python
$ echo -n "password" | openssl sha1
5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8
```

## Chiffrement d'un fichier
```python
$ openssl dgst -md5 -out doc_hashed_altered.txt doc.txt  
$ cat doc_hashed.txt    
MD5(doc.txt)= 96afd0d54c80474a5d4b85394f9451dd  
$ cat doc_hashed_altered.txt    
MD5(doc.txt)= 775696760452a4b66d2a7ddee3ec10d4
```
### Question 3 principe du protocole

1. On envoie le hash d'un fichier
2. Puis le fichier
3. B compare le hash et le fichier envoyé en claire
### Question 4
Fichier mdp en claire :
```
123456  
tourte  
cheval  
chaton  
omelette  
souris  
clavier  
arbre  
pomme  
porte
```

```
e10adc3949ba59abbe56e057f20f883e
7328ded0772e29988f7f50020cbf448d
9f87ec4fda4a91dd564f6bdf1ab301c8
448a6316bde2e65405383cb495d1b2d9
d5eefa915a939c9a1eaafdd3366b2310
c40f92f2538305e75c2a34ca6fb55f33
fdd14a30f1a58fe40c939ad2acf5b2c1
8e6c32949d486c9770b16130011e2ad9
ede0f9c3a1d2093e3f48fcafd3c70915
295c6ed838bd29de4ca205d30e6f5469
```


