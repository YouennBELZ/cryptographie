# Chiffrement Asymétrique
Chiffrement **RSA** basé sur la complexité de factorisation de deux facteur premier. **Courbe elliptique** (vue en master de maths).

**Principe :**
Alice veut envoyer un message m à bob
Bob possède une clé privé et une clé publique
Alice prend la clé publique de Bob et chiffre son message chiffré par cette clé
Bob déchiffre son message avec la clé privé

Chiffrement est **couteux** en temps de calcul (beaucoup plus qu'**AES**), et demande une clé de la taille des données.

Permet de signer afin d'assurer l'intégrité des données et de la provenance de ce fichier.

