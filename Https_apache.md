## Installation Apache
```
sudo apt update
sudo apt install apache2
```

### Vérification
```
sudo systemctl status apache2
```

## Activer SSL
```bash
sudo a2enmod ssl
```

```bash
sudo a2ensite default-ssl
```

## Génération d'un certificat auto signé
```bash
openssl genrsa -out private.key 2048
```
```bash
openssl req -new -key private.key -out csr.pem
```
```bash
openssl x509 -req -days 365 -in csr.pem -signkey private.key -out certificate.pem
```


## Mettre les certifs et modifier les lignes suivantes

```
SSLCertificateFile /etc/ssl/certs/iut_certificate.pem

SSLCertificateKeyFile /etc/ssl/private/iut_private.key
```

## Authentification apache
```bash
sudo apt-get install apache2-utils
```

```bash
sudo htpasswd -c /etc/apache2/.htpasswd user1
```

```bash
sudo htpasswd /etc/apache2/.htpasswd user2
```

